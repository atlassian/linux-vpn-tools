#!/bin/bash

# See https://bitbucket.org/atlassian/linux-vpn-tools
# Docs https://networkmanager.dev/docs/api/latest/NetworkManager-dispatcher.html

if [ "$NM_DISPATCHER_ACTION" = "vpn-up" ]; then
    echo "VPN_IP_IFACE=$VPN_IP_IFACE" > /run/managed-vpn-routing.env
    systemctl restart managed-vpn-routing
elif [ "$NM_DISPATCHER_ACTION" = "vpn-down" ]; then
    rm -f /run/managed-vpn-routing.env
    systemctl stop managed-vpn-routing
    ip link delete "$VPN_IP_IFACE"
fi
