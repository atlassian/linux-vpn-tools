#!/usr/bin/env python3

# See https://bitbucket.org/atlassian/linux-vpn-tools

import logging
import os
import subprocess
import sys
import time

VERSION = '2.0.1'

ENDPOINTS = [
    "aas0641.my.idaptive.app",  # Atlassian prod Cyberark tenant
    "aaq0004.my.idaptive.app",  # Atlassian staging Cyberark tenant
    "admin.google.com",  # ... Google admin interface :-)
    "atlassian.cloud.databricks.com",  # Old Socrates warehouse
    "atlassian.my.centrify.com",  # Old-old name for Cyberark
    "atlassian.my.idaptive.app",  # New-old name for Cyberark
    "atlassian-stg.my.centrify.com",  # Old-old name for staging Cyberark
    "atlassian-stg.my.idaptive.app",  # New-old name for staging Cyberark
    "atlassian-dev.cloud.databricks.com",  # Development interactive Socrates warehouse
    "atlassian-discover.cloud.databricks.com",  # Primary interactive Socrates warehouse
    "atlassian-stg.cloud.databricks.com",  # Staging deployment Socrates warehouse
    "atlassian-prod.cloud.databricks.com",  # Prod deployment Socrates warehouse
    "auth-provider.prod.auth.kitt-inf.net",  # KITT cluster auth
    "atlassian.id.cyberark.cloud",  # Cyberark WPM
    "atlassian.cyberark.cloud",  # Cyberark PAM
    "atlassian-us-gov-mod.oktapreview.com",  # Fedramp staging Okta
    "atlassian-us-gov-mod.okta.com",  # Fedramp prod Okta
]
SLEEP_TIME = 300


def configure_logging():
    logging.root.addHandler(logging.StreamHandler(sys.stdout))
    logging.root.setLevel(logging.INFO)


def add_route(interface, ip):
    subprocess.run(["ip", "route", "add", f"{ip}/32", "dev", interface], check=False)


def remove_route(ip):
    subprocess.run(["ip", "route", "del", f"{ip}/32"], check=False)


def resolve_domain(name):
    ips = []
    hosts_process = subprocess.run(["getent", "hosts", name],
                                   capture_output=True,
                                   timeout=60,
                                   check=True,
                                   text=True)
    for line in hosts_process.stdout.split('\n'):
        line = line.strip()
        if len(line) == 0:
            continue
        ip = line.split()[0]
        ips.append(ip)

    return ips


def calculate_new_ips():
    new_ips = set()

    for endpoint in ENDPOINTS:
        try:
            new_ips.update(resolve_domain(endpoint))
        except subprocess.CalledProcessError as e:
            logging.error("Failed to resolve '%s': %s", endpoint, e)

    return new_ips


def wait_for_network():
    # Wait up 5 attempts for DNS to start working before entering the main loop
    for i in range(5):
        try:
            resolve_domain("atlassian.com")
        except subprocess.CalledProcessError as e:
            logging.info("Waiting for network...")
            time.sleep(1)


def main():
    current_ips = set()
    interface = os.environ.get("VPN_IP_IFACE", None)
    if not interface:
        logging.error("Missing environment variable: VPN_IP_IFACE")
        sys.exit(1)

    wait_for_network()

    logging.info("Managing routes for interface: %s", interface)

    while True:
        new_ips = calculate_new_ips()

        entry_set = new_ips.difference(current_ips)
        exit_set = current_ips.difference(new_ips)
        logging.info("Changed routes: +%s -%s", len(entry_set), len(exit_set))

        for ip in entry_set:
            add_route(interface, ip)
        for ip in exit_set:
            remove_route(ip)

        current_ips = new_ips
        time.sleep(SLEEP_TIME)


if __name__ == "__main__":
    configure_logging()
    main()
