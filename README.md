# Atlassian Linux Help

This repository contains instructions and relevant tooling for setting up (or maintaining) a Linux laptop at Atlassian,
including connecting it to the company VPN.

Linux distributions may vary widely. Although every effort will be made to be distribution-independent, the tested path
for the tooling here is with the latest Ubuntu LTS release. You may need to make modifications for other Linux
distributions.

## Why are these human instructions instead of an automatic tool?

1. You get to choose how you follow the steps, to understand what you're doing
1. You can easily adapt the steps to the distribution of your choice
1. It's super simple to diagnose what went wrong. If you break it you keep the pieces :-)

# Zero to VPN

This describes how to go from a fresh Linux install to a ready-to-work state.

## Prerequisites

- System installed using full-disk encryption (LUKS). This is a company security policy
- You have the ability to generate 2FA codes, via an enrolled company Yubikey, the Duo app, or otherwise
- This repository checked out on your newly acquired workstation. Tick! Gold star!
- The following packages installed:
    1. curl
    1. python3
    1. network-manager
    1. openconnect
    1. network-manager-openconnect-gnome (yes, in addition to network-manager and openconnect)
    1. iwd

## Steps

1. Connect to the [Network and the VPN](docs/network.md).
[Connect to VPN using Linux]: https://hello.atlassian.net/wiki/spaces/ITKB/pages/1206033199/Connect+to+VPN+on+a+corporate+Linux

## Setting up VPN on NixOS

See [./setting-up-vpn-on-nixos.md](./setting-up-vpn-on-nixos.md)
