#!/usr/bin/python3

import sys, subprocess, os, tempfile
from enum import Enum
from typing import Tuple

class TestResult(Enum):
    PASS = 'PASS',
    UNKNOWN = 'WARN',
    FAIL = 'FAIL',

def use_tests(executed, report, *tests) -> Tuple[TestResult, str]:
    for test in tests:
        if test not in executed:
            return TestResult.UNKNOWN, [f'Could not run test {test}']
        if test in report:
            return TestResult.FAIL, report[test]

    return TestResult.PASS, ''

def is_disk_encrypted(executed, report) -> Tuple[TestResult, str]:
    return use_tests(executed, report, 'CRYP-7930', 'CRYP-7931')


def osquery_running(executed, report) -> Tuple[TestResult, str]:
    if not os.path.isfile('/etc/osquery/osquery.flags'):
        return TestResult.FAIL, ["osquery flags file missing"]

    return use_tests(executed, report, 'FINT-4338')


def browser_config_installed(executed, report) -> Tuple[TestResult, str]:
    if not os.path.isfile('/etc/firefox/policies/policies.json'):
        return TestResult.FAIL, ['Firefox browser policy file missing']

    if not os.path.isfile('/etc/opt/chrome/policies/managed/linux.json'):
        return TestResult.FAIL, ['Chrome browser policy file missing']

    if not os.path.isfile('/etc/chromium-browser/policies/managed/linux.json'):
        return TestResult.FAIL, ['Chromium browser policy file missing']

    return TestResult.PASS, ''


def secure_boot_enabled(executed, report) -> Tuple[TestResult, str]:
    call_result = subprocess.check_output(['/usr/bin/mokutil', '--sb-state'], text=True).strip()
    if 'SecureBoot enabled' in call_result:
        return TestResult.PASS, ''
    return TestResult.FAIL, call_result


def gnome_screensaver_running() -> bool:
    call_result = subprocess.check_output(['/usr/bin/ps', 'ax', '-o', 'cmd'], text=True)
    for line in call_result.split('\n'):
        if '/gsd-screensaver-proxy' or '/csd-screensaver-proxy' in line:
            return True
    return False


def screen_lock_enabled(executed, report) -> Tuple[TestResult, str]:
    if not gnome_screensaver_running():
        return TestResult.UNKNOWN, 'not using gnome'

    call_result = subprocess.check_output(['/usr/bin/gsettings', 'get', 'org.gnome.desktop.screensaver',
                                           'lock-enabled'], text=True).strip()
    if call_result != 'true':
        return TestResult.FAIL, f"gnome-screensaver lock-enabled was {call_result}"

    call_result = subprocess.check_output(['/usr/bin/gsettings', 'get', 'org.gnome.desktop.screensaver',
                                           'idle-activation-enabled'], text=True).strip()

    if call_result != 'true':
        return TestResult.FAIL, f"gnome-screensaver idle-activation-enabled was {call_result}"

    return TestResult.PASS, ''


def screen_lock_timeout_short_enough(executed, report) -> Tuple[TestResult, str]:
    if not gnome_screensaver_running():
        return TestResult.UNKNOWN, ['not using gnome']

    call_result = subprocess.check_output(['/usr/bin/gsettings', 'get', 'org.gnome.desktop.screensaver',
                                           'lock-delay'], text=True)
    lock_delay = int(call_result.split()[1])

    call_result = subprocess.check_output(['/usr/bin/gsettings', 'get', 'org.gnome.desktop.session',
                                           'idle-delay'], text=True)
    idle_delay = int(call_result.split()[1])

    if idle_delay + lock_delay <= 300:
        return TestResult.PASS, ''

    return TestResult.FAIL, f"Total delay before locking on inactivity was {idle_delay + lock_delay}, which is > 300"


def firewall_configured(executed, report):
    return use_tests(executed, report, 'FIRE-4502', 'FIRE-4590')


def writable_usb_disabled(executed, report):
    return use_tests(executed, report, 'USB-1000', 'STRG-1846')


checks = [
    is_disk_encrypted,
    osquery_running,
    browser_config_installed,
    secure_boot_enabled,
    screen_lock_enabled,
    screen_lock_timeout_short_enough,
    firewall_configured,
    writable_usb_disabled
]


class Colors:
    OK = '\033[92m'
    WARN = '\033[93m'
    FAIL = '\033[91m'
    RESET = '\033[0m'


def run_lynis_report(tempdir: str):
    tests = [
        'USB-1000',
        'FIRE-4502',
        'FIRE-4590',
        'CRYP-7930',
        'CRYP-7931',
        'STRG-1846',
        'FINT-4338',
        'BOOT-5116',
        'BOOT-5184'
    ]
    report_file = os.path.join(tempdir, 'report.dat')
    subprocess.check_call(['lynis', 'audit', 'system', '--no-log', '-q', '--report-file', report_file,
                           '--tests', ','.join(tests)])

    suggestion_marker = 'suggestion[]='
    tests_executed_marker = 'tests_executed='

    report = {}
    executed = None
    with open(report_file, 'r') as f:
        for line in f:
            if line.startswith(tests_executed_marker):
                line = line[len(tests_executed_marker):]
                executed = [x for x in line.strip().split('|') if len(x) > 0]
                continue
            if not line.startswith(suggestion_marker):
                continue
            line = line[len(suggestion_marker):]
            suggestion_source, text = line.split('|')[:2]
            if suggestion_source not in report:
                report[suggestion_source] = []
            report[suggestion_source].append(text)

    os.remove(report_file)

    if executed is None:
        raise Exception("Lynis didn't run any tests!")

    return executed, report


if __name__ == '__main__':
    if os.geteuid() != 0:
        sys.stderr.write(f"{Colors.WARN}Script doesn't appear to be running as root - "
                         f"possibly won't be able to get accurate check results{Colors.RESET}\n")
        sys.stderr.flush()

    if not os.path.isfile('/usr/sbin/lynis'):
        sys.stderr.write(f"{Colors.WARN}You don't seem to have Lynis installed - "
                         f"that's needed for many checks{Colors.RESET}\n")
        sys.stderr.flush()

    with tempfile.TemporaryDirectory() as tempdir:
        executed, report = run_lynis_report(tempdir)

        for check in checks:
            sys.stdout.write(f'{check.__name__}: ')
            result = None
            try:
                result, suggestion = check(executed, report)
            except Exception as e:
                result = TestResult.UNKNOWN
                suggestion = repr(e)

            if result == TestResult.PASS:
                sys.stdout.write(f'{Colors.OK}PASS{Colors.RESET}\n')
            elif result == TestResult.FAIL:
                sys.stdout.write(f'{Colors.FAIL}FAIL{Colors.RESET}: {suggestion}\n')
            else:
                sys.stdout.write(f'{Colors.WARN}UNKNOWN{Colors.RESET}: {suggestion}\n')
