This page describes how to set up the Atlassian office network and the corporate VPN.

# Connect to the office wireless network

If you're always working from home, you can skip this section and connect to your home network instead. But if you
plan to be in the office, you'll need it...

Optionally, you can [Switch to iwd](iwd.md) for a better in-office wifi experience.

## Create Charlie Network Profile

The in-office wifi network uses the SSID "Charlie".

Use the following settings to connect:

- Network Name: Charlie
- Wi-Fi Security: WPA & WPA2 Enterprise
- Authentication Method: Protected EAP (PEAP)
- Inner Authentication: MS-CHAP-v2
- Username: your StaffID username
- Password: your StaffID password
- CA Certificate: Select the file `Atlassian-Office-Root-CA2.pem`

You should now be connected to the office wifi!

# Connect to the VPN

Now that you have the office (or other) network set up, let's connect to the VPN.

## Get System Added to ZeroTrust Exceptions List

In order to connect to the VPN, your system must be added to a "Zero Trust Exceptions" list. This consists of adding
your system's wireless MAC address to a database. You can find your MAC address(es) with:

```shell
for dev in /sys/class/net/*/device; do cat "$(dirname "$dev")/address"; done
```

For tracking, you'll also need to submit your system serial number:
```shell
sudo lshw -class system | grep serial
```

Raise a ticket asking for a ZeroTrust exception. Don't ask how you're supposed to do that if this is your first
company computer. Figure out a way to get in touch with Workplace Tech through your colleagues or otherwise.

## Install VPN Routing Scripts

Unfortunately, some software Atlassian uses is hosted on computers whose IP addresses "float", changing over time, but
also requires that you use the VPN in order to access it. To deal with that situation, this script will periodically
check for changes in the IP addresses of those hosts, and add routes to send traffic destined for them over the VPN.

```shell
sudo install "$PWD/99-managed-vpn-routing.sh" /etc/NetworkManager/dispatcher.d/
sudo ln -s "$PWD/managed-vpn-routing.py" /usr/local/bin/
sudo ln -s "$PWD/managed-vpn-routing.service" /etc/systemd/system/
```

If you don't configure these scripts, you won't ever be allowed access to a High Trust System, because Cyberark/Idaptive
itself, the login system, is one of those floating endpoints.

## Ensure the right hostname is sent

Unfortunately, the Atlassian VPN servers will reject connections if they don't use the User-Agent string `AnyConnect`.
By default, `openconnect` sends its own user-agent. If you don't fix that, you will receive a nondescript 404 response
on trying to connect to the VPN.

With a sufficiently new version of the NetworkManager OpenConnect plugin, you can specify the user-agent as part of the
VPN connection properties. You're welcome to skip to "Add VPN Connection" below and try. If you don't see an option
for that, you will need to run the following command:

```shell
sed -i 's/OpenConnect VPN Agent/AnyConnect\x001234512345/' /usr/lib/NetworkManager/nm-openconnect-auth-dialog
```

After each and every upgrade of the NetworkManager OpenConnect plugin, **you will need to rerun this command or lose
VPN access**.

## Add VPN connection

We're finally ready to create a new VPN connection with the following settings:

- VPN Protocol: Cisco AnyConnect or openconnect
- Gateway: vpn.atlassian.com/exception
- CA certificate: <leave blank; there is no client certificate auth and the server uses a public CA-issued certificate>
- Check the box "Allow security scanner trojan (CSD)"
- Trojan (CSD) Wrapper Script: choose the path to the `csd-post` file from this repository
- Software Token Authentication - Token Mode: HOTP -- manually entered
- User Agent (if offered): `AnyConnect`
- IPv4 Settings -> Additional search domains: `atlassian.com, ~.`

The trojan/csd-post script will send Atlassian's VPN server your system's host name and a bit of other information. It
pretends to have run a "security posture check" on your machine. Without it, you won't be allowed on the VPN. Don't
think about this one too hard; just make sure you're on the exceptions list and the script is in place.

Click the connect button. You should get a prompt asking for your username and *two* passwords.

- Username: your staff ID username
- Password 1: your staff ID password
- Password 2: either a duo six-digit HOTP code, or a yubikey OTP string, or the word "push". If you type "push" you
  will get an app push notification to confirm the VPN connection

You should now be on the VPN!