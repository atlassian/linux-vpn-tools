# Switch to `iwd`

This page describes how to switch from the default `wpa_supplicant` to `iwd`.

Using `iwd` instead of the default `wpa_supplicant` for the office network can
reduce random network disconnections and improve the overall experience.

Create a file, `/etc/NetworkManager/conf.d/iwd.conf`, with the following content:

```shell
[device]
wifi.backend=iwd
```

Create a file, `/var/lib/iwd/Charlie.8021x`, with content like the following:

```shell
[IPv6]
Enabled=true

[Security]
EAP-Method=PEAP
EAP-Identity=<your Staff ID username>
EAP-PEAP-Phase2-Method=MSCHAPV2
EAP-PEAP-Phase2-Identity=<your Staff ID username>
EAP-PEAP-Phase2-Password=<your Staff ID password>
EAP-PEAP-CACert=/var/lib/iwd/Atlassian-Office-Root-CA2.pem
```

Run the following to disable `wpa_supplicant`:

```shell
sudo systemctl mask wpa_supplicant
```

Finally, reboot your computer to make sure NetworkManager is restarted.

**If you created a wireless network profile before switching to iwd, you will need to delete and re-create it**.
