# Setting up VPN on NixOS

## Requirements

Ensure you have enabled flakes in your Nix configuration. See [Enable flakes | Flakes | NixOS](https://nixos.wiki/wiki/Flakes#NixOS)

## Quick start

To use this flake, you need to import the modules into your main configuration flake.

E.g.

```nix
{
    inputs = {
      vpn = {
        url = "<url-to-csd-post-repo>";
      };
    };

    outputs = { self, ... }@inputs: {
      nixosModules.default = { lib, ... }: {
        imports = [
          inputs.vpn.nixosModules.default
          (inputs.vpn.lib.mkNetworkManagerVpnModule {
            interfaceName="<network-interface-name>";
            atlassianUsername="<your-atlassian-username>";
          })
        ];
      };
    };
}
```

Replace:
- `<url-to-csd-post-repo>` with the path to the csd repo. E.g. `git+file:/home/pshaw/code/csd-post`
- `<network-interface-name>` with your Wifi device name. E.g. `wlan0`
  - Can be found with `ifconfig`
- `<your-atlassian-username>` The username that Atlassian assigned you. E.g. `pshaw`

Note that this provides a relatively minimalist configuration. You may still want to install packages such as [networkmanagerapplet](https://search.nixos.org/packages?show=networkmanagerapplet&from=0&size=50&sort=relevance&type=packages&query=networkmanagerapplet).

## Not using flakes yet?

See [Using flakes with stable Nix | Flakes | NixOS](https://nixos.wiki/wiki/Flakes#Using_flakes_with_stable_Nix) 