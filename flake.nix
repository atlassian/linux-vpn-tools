# See: ./setting-up-vpn-on-nixos.md
{
  inputs = {
    openconnectPatchFile = {
      url = "https://gitlab.gnome.org/GNOME/NetworkManager-openconnect/-/commit/b5e154c06fd9013a925f85c2aa38d88e4ee53db0.diff";
      flake = false;
    };
  };
  outputs = { self, ... }@inputs: {  
    overlays = {
      # See: https://atlassian.slack.com/archives/CFGPGM49Z/p1682945704809769
      openconnectPatch = (self: super: {
        gnome = super.gnome // {
          networkmanager-openconnect = super.gnome.networkmanager-openconnect.overrideAttrs (old: {
            patches = (old.patches or []) ++ [inputs.openconnectPatchFile];
          });
        };
      });
    };

    lib.mkNetworkManagerWifiModule = {
      atlassianUsername,
      zone ? "work"
    }: { pkgs, lib, ... }: {
      environment.etc = let
      in {
        "NetworkManager/system-connections/Charlie.nmconnection" = {
          mode = "0600";
          group = "users";
          text = ''
            [connection]
            id=Charlie
            uuid=0f495763-3de8-4244-8ae7-5528ff174f00
            type=wifi
            zone=${zone}

            [wifi]
            mode=infrastructure
            ssid=Charlie

            [wifi-security]
            key-mgmt=wpa-eap

            [802-1x]
            eap=peap;
            identity=${atlassianUsername}
            phase2-auth=mschapv2
            password-flags=1
            ca-path=${self}/Atlassian-Office-Root-CA2.pem

            [ipv4]
            method=auto

            [ipv6]
            addr-gen-mode=stable-privacy
            method=auto

            [proxy]
          '';
        };
      };
    };

    # Creates a network manager profile for your VPN. Based on: https://hello.atlassian.net/wiki/spaces/ITKB/pages/1206033199/Connect+to+VPN+on+a+corporate+Linux#Arch-Linux
    lib.mkNetworkManagerVpnModule = { 
      interfaceName,
      atlassianUsername,
      profileUuid ? "5c43bd96-9220-45ac-b6ba-5ca67c0fee54"
    }: { pkgs, lib, ... }: {
      # Should be kept in sync with managed-vpn-routing.service
      systemd.services.managed-vpn-routing = let 
        runtimeInputs = [ pkgs.python3 pkgs.getent pkgs.iproute2 pkgs.coreutils-full pkgs.util-linux pkgs.bash ];
        manage-vpn-routing-py = pkgs.writeTextFile {
          name = "manage-vpn-routing-python.sh";
          text = ''
            #!${pkgs.bash}/bin/bash
            export PATH="${lib.makeBinPath runtimeInputs}:$PATH"

            exec python3 ${self}/managed-vpn-routing.py
          '';
          executable = true;
        };
      in {
        description = "Route dynamic hosts' IPs over VPN connection";
        serviceConfig = {
          EnvironmentFile="/run/managed-vpn-routing.env";
          ExecStart="${manage-vpn-routing-py}";
          Restart="always";
        };
      };
    
      environment.etc = let
        runtimeInputs = [
          # Script itself gets generated for these packages under via python3
          pkgs.python3

          # The python script calls curl
          pkgs.curl
        ];
        csdPost = pkgs.writeTextFile {
          name = "csd-post.sh";
          text = ''
            #!${pkgs.bash}/bin/bash
            export PATH="${lib.makeBinPath runtimeInputs}:$PATH"

            exec python3 ${self}/csd-post "$@"
          '';
          executable = true;
        };        
      in {
        "NetworkManager/system-connections/Atlassian.nmconnection" = {
          mode = "0600";
          group = "users";
          text = ''
            [connection]
            id=Atlassian
            uuid=${profileUuid}
            type=vpn
            interface-name=${interfaceName}

            [vpn]
            authtype=password
            cookie-flags=6
            csd_wrapper=${csdPost}
            enable_csd_trojan=yes
            gateway=vpn.atlassian.com/exception
            gateway-flags=6
            gwcert-flags=6
            lasthost-flags=6
            prevent_invalid_cert=no
            protocol=anyconnect
            resolve-flags=6
            stoken_Source=disabled
            useragent=AnyConnect
            xmlconfig-flags=6
            password-flags=1
            persistent=TRUE
            secondary_password-flags=6
            service-type=org.freedesktop.NetworkManager.openconnect
            
            [vpn-secrets]
            form:main:username=${atlassianUsername}

            [ipv4]
            dns-search=~bitbucket.org.;~atlassian.com.;office.atlassian.com;
            method=auto

            [ipv6]
            addr-gen-mode=stable-privacy
            ip6-privacy=0
            method=disabled
          '';
        };
      };
    };

    # Sets the network manager dispatchers for the VPN
    nixosModules.networkManagerDispatchers = { pkgs, lib, ... }: let
      runtimeInputs = [ pkgs.iproute2 pkgs.bash ];
      vpn-dispatch = pkgs.writeTextFile {
        name = "vpn-dispatch.sh";
        text = ''
          #!${pkgs.bash}/bin/bash
          export PATH="${lib.makeBinPath runtimeInputs}:$PATH"

          source ${self}/99-managed-vpn-routing.sh
        '';
        executable = true;
      };        
    in {
      networking.networkmanager.dispatcherScripts = [
        {
          source = vpn-dispatch;
          type = "basic";
        }
      ];
    };

    # Patches openconnect so it works with the Atlassian VPN
    nixosModules.openconnect = { pkgs, ... }: {
      nixpkgs.overlays = [
        # No longer needed on newer versions of Nix
        # self.overlays.openconnectPatch
      ];
      
      environment.systemPackages = [pkgs.openconnect];
    };

    # Settings if you're using networkmanagers
    nixosModules.networkmanager = { pkgs, ... }: {
      networking.networkmanager = {
        enable = true;
        plugins = [
          pkgs.gnome.networkmanager-openconnect
        ];
      };

      imports = [
        self.nixosModules.openconnect

        self.nixosModules.networkManagerDispatchers
      ];
    };

    # Import this + lib.mkNetworkManagerVpnModule if you're not sure what you're doing
    nixosModules.default = self.nixosModules.networkmanager;
  };
}
